<!-- Sign up form -->
<section class="signup">

    <div class="signup-content">
        <div class="signup-form">
            <h2 class="form-title">Registrar</h2>
            <form method="POST" class="register-form" id="register-form" action="includes\cadastro_usuario.php" name="cadastro">


                <div class="form-group">
                    <label for="usuario"><i class="zmdi zmdi-account material-icons-name"></i></label>
                    <input type="text" name="usuario" id="usuario" placeholder="Seu Usuario" />
                </div>

                <div class="form-group">
                    <label for="Senha"><i class="zmdi zmdi-lock"></i></label>
                    <input type="password" name="senha" id="pass" placeholder="Senha" />
                </div>
                <div class="form-group">
                    <label for="re-pass"><i class="zmdi zmdi-lock-outline"></i></label>
                    <input type="password" name="conf_senha" id="re_pass" placeholder="Confirme a senha" />
                </div>
                <div class="form-group form-button">
                    <input type="submit" name="signup" id="signup" class="form-submit" value="Cadastrar" />
                </div>
            </form>
        </div>
    </div>

</section>