<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Sign Up Form by Colorlib</title>

  <!-- Font Icon -->
  
  <!-- Main css -->
  <link rel="stylesheet" href="node_modules/bootstrap/compiled/bootstrap.css">
  <!-- <link rel="stylesheet" href="css/style.css"> -->
  <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->

</head>

<body>

  <nav class="navbar navbar bg-dark">
    <a class="navbar-brand" href="#">
      <img src="./img/detran-marca.svg" width="200" height="60" class="d-inline-block align-top" alt="">

    </a>
    <ul class="nav nav-tabs bg-dark">
      <li class="nav-item">
        <a class="nav-link " href="#">Active</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Another link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
  </nav>


<!-- primary -->
  <div class="container">
    <div class="row">

      <div class="col-6" style="text-align: left;">
        <?php
        include('registro_ferias.php');
        ?>
      </div>
      <div class="col-6">
        <div class="row">
          <?php 
          include('login.php');
          ?>
        </div>
      </div>

    </div>
  </div>

  <!-- JS -->
  <script src="node_modules/jquery/dist/jquery.js"></script>
  <script src="node_modules/popper.js/dist/popper.js"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>