<legend>Login</legend>
<section class="sign-in">
    <div class="container">
        <div class="signin-content">
           
            <div class="signin-form">
                <h2 class="form-title">Entrar</h2>
                <form method="POST" class="register-form" id="login-form" action="includes\session.php">
                    <div class="form-group">
                        <label for="usuario">Usuário:</label>
                        <input type="text" name="usuario" id="usuario" class="form-control" placeholder="Usuario" required />
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha:</label>
                        <input type="password" name="senha" id="senha" class="form-control" placeholder="Senha" required />
                    </div>

                    <div class="form-group form-button">
                        <input type="submit" name="signin" id="signin" class="form-submit" value="Log in" />
                    </div>
                </form>
               
            </div>
        </div>
    </div>
</section>